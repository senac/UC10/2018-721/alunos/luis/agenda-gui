package banco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexão {

    private static final String URL = "jdbc:mysql://localhost/agenda" ; 
    private static final String USER = "root";
    private static final String PASSWORD = "123456";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    public static Connection getConnection() {

        Connection connection = null;
        try {
            Class.forName(DRIVER);
            connection = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("conectou!!!!!!");

        } catch (ClassNotFoundException ex) {
            System.out.println("erro ao carregar o diver do banco");
            ex.printStackTrace();

        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("erro ao conectar no banco");
        }

        return connection;

    }
    
    
    public static void main(String[] args) {
        Conexão.getConnection();
    }

}
