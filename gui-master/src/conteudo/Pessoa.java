package conteudo;

import javax.swing.JFormattedTextField;
import javax.swing.JTextField;


public class Pessoa {

    private String nome;
    private String telefone;

    public Pessoa(String nome, String telefone) {
        this.nome = nome;
        this.telefone = telefone;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefone() {
        return this.telefone;
    }

    @Override
    public String toString() {
      
        return "Nome: " + this.nome + " - " + "Telefone: " + this.telefone;
    }

    @Override
    public boolean equals(Object outraPessoa) {

        if (outraPessoa instanceof Pessoa) {
          
            Pessoa p = (Pessoa) outraPessoa;

            return this.nome.equals(p.getNome()) && this.telefone.equals(p.getTelefone());

        }

        return false;
    }

    public void setNome(JTextField jTextFieldNome) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setTelefone(JFormattedTextField jFormattedTextFieldtelefone) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
